package net.ihe.gazelle.matchbox.client.interlay.validation;

import net.ihe.gazelle.matchbox.client.interlay.validation.mock.CustomPatientValidationServiceMock;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationReport;
import net.ihe.gazelle.validation.api.domain.request.structure.ValidationItem;
import net.ihe.gazelle.validation.api.domain.request.structure.ValidationRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

class CustomPatientValidationServiceTest {

    static CustomPatientValidationService customPatientValidationService = new CustomPatientValidationServiceMock();


    static ValidationRequest request = new ValidationRequest();
    static ValidationItem item = new ValidationItem();
    static File patientFranzFile = new File("src/test/resources/post_request_passed.json");
    static String patientFranz;


    @BeforeAll
    static public void initialize() {
        try {
            patientFranz = Files.readString(Path.of(patientFranzFile.getAbsolutePath()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        item.setContent(patientFranz.getBytes());
        request.addValidationItem(item);

    }

    @Test
    void validatePassed() {
        request.setValidationProfileId("passed");
        ValidationReport report = customPatientValidationService.validate(request);
        Assertions.assertEquals("PASSED", report.getOverallResult().name());
    }

    @Test
    void validateFailed() {
        request.setValidationProfileId("failed");
        ValidationReport report = customPatientValidationService.validate(request);
        Assertions.assertEquals("FAILED", report.getOverallResult().name());
    }

    @Test
    void getValidationProfiles() {
        Assertions.assertInstanceOf(ArrayList.class, customPatientValidationService.getValidationProfiles());
    }
}