package net.ihe.gazelle.business.provider.mock;

import ca.uhn.fhir.rest.server.exceptions.UnprocessableEntityException;
import net.ihe.gazelle.app.patientregistryapi.application.SearchCrossReferenceException;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryxrefsearchclient.adapter.XRefSearchClient;
import net.ihe.gazelle.application.PatientRegistryXRefSearchClient;
import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesService;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Parameters;
import org.hl7.fhir.r4.model.Parameters.ParametersParameterComponent;

import java.util.List;

public class PatientRegistryXRefSearchClientMock extends PatientRegistryXRefSearchClient {
    public static final String HTTP_OK = "HTTP_OK";
    public static final String URN_OK = "URN_OK";
    public static final String ERROR_DURING_SEARCH_CROSS_REFERENCE = "Error during Search cross reference";
    public static final String IS_NULL = "is null";
    private static final String CROSS_REFERENCE = "Cross Reference";

    public PatientRegistryXRefSearchClientMock() {
    }

    public PatientRegistryXRefSearchClientMock(OperationalPreferencesService operationalPreferencesService) {
        super(operationalPreferencesService);
    }

    public PatientRegistryXRefSearchClientMock(XRefSearchClient xRefSearchClient) {
        setClient(xRefSearchClient);
    }

    @Override
    public Parameters process(EntityIdentifier sourceIdentifier, List<String> targetSystemList) throws SearchCrossReferenceException {
        Parameters response = new Parameters();
        ParametersParameterComponent alias = new ParametersParameterComponent();
        Identifier r4Identifier = new Identifier();
        if (sourceIdentifier == null) {
            throw new UnprocessableEntityException(IS_NULL);
        }
        switch (sourceIdentifier.getSystemIdentifier()) {
            case "http://1" -> {
                r4Identifier.setSystem(HTTP_OK);
                r4Identifier.setValue(HTTP_OK);
                alias.setValue(r4Identifier);
                alias.setName(CROSS_REFERENCE);
                response.addParameter(alias);
                return response;
            }
            case "1" -> {
                r4Identifier.setSystem(URN_OK);
                r4Identifier.setValue(URN_OK);
                alias.setValue(r4Identifier);
                alias.setName(CROSS_REFERENCE);
                response.addParameter(alias);
                return response;
            }
            case "0" -> throw new SearchCrossReferenceException(ERROR_DURING_SEARCH_CROSS_REFERENCE);
            default -> {
                throw new UnprocessableEntityException(IS_NULL);
            }
        }
    }
}
