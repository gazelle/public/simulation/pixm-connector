package net.ihe.gazelle.business.provider.mock;

public class HttpFormatException extends Exception {
    public HttpFormatException(String message) {
        super(message);
    }
}
