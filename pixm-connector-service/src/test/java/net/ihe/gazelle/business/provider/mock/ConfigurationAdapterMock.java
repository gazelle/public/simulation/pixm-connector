package net.ihe.gazelle.business.provider.mock;

import net.ihe.gazelle.application.ConfigurationAdapter;

public class ConfigurationAdapterMock extends ConfigurationAdapter {


    @Override
    public String getProfileIdCreateUpdateDeleteIti104() {
        return "IHE_ITI-104-PatientFeed_Query";
    }

    public String getProfileIdPostIti83() {
        return "IHE_ITI-83_GET_PIXm_Query";
    }

    public String getProfileIdGetIti83() {
        return "IHE_ITI-83_GET_PIXm_Query";
    }
}
