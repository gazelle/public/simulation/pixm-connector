package net.ihe.gazelle.business.provider.mock;


import com.fasterxml.jackson.databind.json.JsonMapper;
import jakarta.servlet.http.HttpServletRequest;
import net.ihe.gazelle.business.service.RequestValidatorService;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationReport;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationTestResult;
import net.ihe.gazelle.validation.api.domain.request.structure.ValidationItem;
import net.ihe.gazelle.validation.interlay.dto.report.ValidationReportDTO;
import org.hl7.fhir.r4.model.Resource;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static net.ihe.gazelle.matchbox.client.interlay.validation.CustomPatientValidationService.MATCHBOX_VALIDATION;

public class RequestValidatorServiceMock extends RequestValidatorService {

    String pathToReport = "src/test/resources/validationReport.json";
    String operationOutcomePath = "src/test/resources/operationOutcome.json";
    List<ValidationItem> validationItemList = new ArrayList<>();

    public List<ValidationReport> validateRequest(HttpServletRequest request, Resource resource, String profileId) {
        ValidationReport vr = getValidationReport();
        List<ValidationReport> lvr = new ArrayList<>();
        if (resource != null && "FAILED_VALIDATION".equals(resource.getId())) {
            vr.setOverallResult(ValidationTestResult.FAILED);
            addMessageToValidationItem(getOperationOutcomeAsString());
            vr.setValidationItems(validationItemList);
            lvr.add(vr);
        }
        return lvr;
    }



    private ValidationReport getValidationReport() {
        ValidationReport vr = new ValidationReport();
        try {
            JsonMapper jsonMapper = new JsonMapper();
            String content = Files.readString(Path.of(pathToReport), StandardCharsets.UTF_8);
            vr = jsonMapper.readValue(content, ValidationReportDTO.class);
        } catch (Exception e) {
            System.out.println("Problem while getting file content: " + e.getMessage());
        }
        return vr;
    }

    private void addMessageToValidationItem(String message) {
        ValidationItem vi = new ValidationItem();
        vi.setItemId(MATCHBOX_VALIDATION);
        vi.setContent(message.getBytes(StandardCharsets.UTF_8));
        validationItemList.add(vi);
    }


    private String getOperationOutcomeAsString() {
        String s = "";
        try {
            s = Files.readString(Path.of(operationOutcomePath), StandardCharsets.UTF_8);
        } catch (Exception e) {
            System.out.println("Problem while getting file content: " + e.getMessage());
        }
        return s;

    }


}
