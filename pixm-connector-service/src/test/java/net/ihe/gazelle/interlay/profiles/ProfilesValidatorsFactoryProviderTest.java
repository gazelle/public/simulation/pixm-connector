package net.ihe.gazelle.interlay.profiles;

import ca.uhn.fhir.rest.server.exceptions.UnprocessableEntityException;
import net.ihe.gazelle.application.ProfilesValidatorsFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import uk.org.webcompere.systemstubs.environment.EnvironmentVariables;
import uk.org.webcompere.systemstubs.jupiter.SystemStub;
import uk.org.webcompere.systemstubs.jupiter.SystemStubsExtension;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SystemStubsExtension.class)
class ProfilesValidatorsFactoryProviderTest {

    public static final String UNPROCESSABLE_ENTITY_EXCEPTION_HAS_TO_BE_THROWN = "Unprocessable Entity Exception has to be thrown";

    ProfilesValidatorsFactoryProvider factoryProvider = new ProfilesValidatorsFactoryProvider();
    Map<String, String> mapOfEnv = Map.of("PROFILE_ID_POST_ITI_83", "IHE_ITI-83_POST_PIXm_Query",
            "PROFILE_ID_GET_ITI_83", "IHE_ITI-83_GET_PIXm_Query",
            "PROFILE_ID_CREATE_UPDATE_DELETE_ITI_104", "IHE_ITI-104-PatientFeed_Query");
    @SystemStub
    private EnvironmentVariables environmentVariables = new EnvironmentVariables(mapOfEnv);


    @Test
    void testCreateProfileValidatorFactoryIti104() throws Exception {
        //Given
        String profile = "IHE_ITI-104-PatientFeed_Query";
        //When
        ProfilesValidatorsFactory profilesValidatorsFactory = environmentVariables.execute(() -> factoryProvider.createProfileValidatorFactory(profile));
        //Then
        assertInstanceOf(ITI104PatientFeedQueryProfile.class, profilesValidatorsFactory.createProfileValidator());
    }

    @Test
    void testCreateProfileValidatorFactoryIti83Get() throws Exception {
        //Given
        String profile = "IHE_ITI-83_GET_PIXm_Query";
        //When
        ProfilesValidatorsFactory profilesValidatorsFactory = environmentVariables.execute(() -> factoryProvider.createProfileValidatorFactory(profile));
        //Then
        assertInstanceOf(ITI83GetPIXmQueryProfile.class, profilesValidatorsFactory.createProfileValidator());
    }

    @Test
    void testCreateProfileValidatorFactoryIti83Post() throws Exception {
        //Given
        String profile = "IHE_ITI-83_POST_PIXm_Query";
        //When
        ProfilesValidatorsFactory profilesValidatorsFactory = environmentVariables.execute(() -> factoryProvider.createProfileValidatorFactory(profile));
        //Then
        assertInstanceOf(ITI83PostPIXmQueryProfile.class, profilesValidatorsFactory.createProfileValidator());
    }

    @Test
    void testCreateProfileValidatorFactoryUnknownProfile() throws Exception {
        //Given
        String profile = "unknown";
        //When
        try {
            environmentVariables.execute(() -> factoryProvider.createProfileValidatorFactory(profile));
        } catch (UnprocessableEntityException e) {
            assertEquals(ProfilesValidatorsFactoryProvider.NO_VALIDATOR_FOUND_FOR_THIS_PROFIL_ID + profile, e.getMessage());
        } catch (Exception e) {
            fail(UNPROCESSABLE_ENTITY_EXCEPTION_HAS_TO_BE_THROWN);
        }


    }
}