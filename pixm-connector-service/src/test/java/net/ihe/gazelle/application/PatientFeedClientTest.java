package net.ihe.gazelle.application;

import ca.uhn.fhir.rest.server.exceptions.InternalErrorException;
import ca.uhn.fhir.rest.server.exceptions.InvalidRequestException;
import ca.uhn.fhir.rest.server.exceptions.ResourceGoneException;
import io.qameta.allure.*;
import net.ihe.gazelle.adapter.connector.ConversionException;
import net.ihe.gazelle.app.patientregistryapi.application.PatientFeedException;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryapi.business.GenderCode;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryapi.business.PersonName;
import net.ihe.gazelle.app.patientregistryfeedclient.adapter.PatientFeedClient;
import net.ihe.gazelle.app.patientregistryfeedclient.adapter.PatientFeedProcessResponseException;
import net.ihe.gazelle.framework.preferencesmodelapi.application.NamespaceException;
import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesService;
import net.ihe.gazelle.framework.preferencesmodelapi.application.PreferenceException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import jakarta.xml.ws.WebServiceException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

@Feature("PatientFeedClient")
public class PatientFeedClientTest {

    private static final String TEST_UUID = "123e4567-e89b-12d3-a456-426614174000";
    private static final String MALFORMED_UUID = "123e4567-e89b-12d3-a456-42661417400000000000000000000000000";

    @Mock
    static private PatientFeedClient patientFeedClientMock;
    @Mock
    static private OperationalPreferencesService operationalPreferencesService;
    @Mock
    private PatientRegistryFeedClient patientRegistryFeedClient;

    @BeforeAll
    static public void initialize() {
        patientFeedClientMock = Mockito.mock(PatientFeedClient.class);
        operationalPreferencesService = Mockito.mock(OperationalPreferencesService.class);
    }

    @Test
    @Description("Test on initialization, when a namespace exception is thrown")
    @Severity(SeverityLevel.CRITICAL)
    @Story("initialization")
    public void TestInitializeNameSpaceException() throws PreferenceException, NamespaceException {

        patientRegistryFeedClient = new PatientRegistryFeedClient(operationalPreferencesService);

        Mockito.doThrow(NamespaceException.class).when(operationalPreferencesService).getStringValue("java:/app/gazelle/pixm-connector" +
                "/operational" +
                "-preferences", "patientregistry.url");

        Patient patient = new Patient();

        assertThrows(PatientFeedException.class,
                () -> patientRegistryFeedClient.createPatient(patient));
    }

    @Test
    @Description("Test on initialization, when a preference exception is thrown")
    @Severity(SeverityLevel.CRITICAL)
    @Story("initialization")
    public void TestInitializePreferenceException() throws PreferenceException, NamespaceException {
        Mockito.doThrow(PreferenceException.class).when(operationalPreferencesService).getStringValue("java:/app/gazelle/pixm-connector" +
                "/operational" +
                "-preferences", "patientregistry.url");
        patientRegistryFeedClient = new PatientRegistryFeedClient(operationalPreferencesService);

        Patient patient = new Patient();

        assertThrows(PatientFeedException.class,
                () -> patientRegistryFeedClient.updatePatient(patient, null));
    }

    @Test
    @Description("Test on initialization, when the url of the server is malformed")
    @Severity(SeverityLevel.CRITICAL)
    @Story("initialization")
    public void TestInitializeMalformedURLException() throws PreferenceException, NamespaceException {
        Mockito.doThrow(PreferenceException.class).when(operationalPreferencesService).getStringValue("java:/app/gazelle/pixm-connector" +
                "/operational" +
                "-preferences", "patientregistry.url");
        patientRegistryFeedClient = new PatientRegistryFeedClient(operationalPreferencesService);

        assertThrows(PatientFeedException.class,
                () -> patientRegistryFeedClient.delete(""));
    }

    @Test
    @Description("Test on initialization, when a web service exception is thrown")
    @Severity(SeverityLevel.CRITICAL)
    @Story("initialization")
    public void TestInitializeWebServiceException() throws PreferenceException, NamespaceException {
        Mockito.doThrow(WebServiceException.class).when(operationalPreferencesService).getStringValue("java:/app/gazelle/pixm-connector" +
                "/operational" +
                "-preferences", "patientregistry.url");
        patientRegistryFeedClient = new PatientRegistryFeedClient(operationalPreferencesService);

        assertThrows(PatientFeedException.class,
                () -> patientRegistryFeedClient.mergePatient("", ""));
    }

    @Test
    @Description("Test on create, nominal case")
    @Severity(SeverityLevel.CRITICAL)
    @Story("create")
    public void TestNominalCreation() throws PatientFeedException, ConversionException {

        Patient patient = createPatient("name", "name", LocalDate.of(1990, 06, 19), GenderCode.MALE);

        patientRegistryFeedClient = new PatientRegistryFeedClient();
        patientRegistryFeedClient.setClient(patientFeedClientMock);

        Mockito.doReturn(TEST_UUID).when(patientFeedClientMock).createPatient(any());
        assertEquals(TEST_UUID, patientRegistryFeedClient.createPatient(patient).getId());
    }

    @Test
    @Description("Test on create, null Patient")
    @Severity(SeverityLevel.CRITICAL)
    @Story("create")
    public void TestNullPatientException() throws PatientFeedException {

        Patient patient = null;

        patientRegistryFeedClient = new PatientRegistryFeedClient();
        patientRegistryFeedClient.setClient(patientFeedClientMock);

        assertThrows(InvalidRequestException.class, () -> patientRegistryFeedClient.createPatient(patient));
        try {
            patientRegistryFeedClient.createPatient(patient);
        } catch (InvalidRequestException | ConversionException e) {
            assertEquals(PatientRegistryFeedClient.NO_PATIENT_PARAMETER, e.getMessage());
        }
    }

    @Test
    @Description("Test on create, when a blank uuid is returned")
    @Severity(SeverityLevel.CRITICAL)
    @Story("create")
    public void TestBlankUuidReturnedException() throws PatientFeedException {

        Patient patient = createPatient("name", "name", LocalDate.of(1990, 06, 19), GenderCode.MALE);

        patientRegistryFeedClient = new PatientRegistryFeedClient();
        patientRegistryFeedClient.setClient(patientFeedClientMock);

        Mockito.doReturn("").when(patientFeedClientMock).createPatient(any());
        assertThrows(InternalErrorException.class, () -> patientRegistryFeedClient.createPatient(patient));
        try {
            patientRegistryFeedClient.createPatient(patient);
        } catch (InternalErrorException e) {
            assertEquals(PatientRegistryFeedClient.NO_UUID, e.getMessage());
        } catch (ConversionException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    @Description("Test on create, when a Malformed UUID is returned")
    @Severity(SeverityLevel.CRITICAL)
    @Story("create")
    public void TestMalformedUuidReturnedException() throws PatientFeedException {

        Patient patient = createPatient("name", "name", LocalDate.of(1990, 06, 19), GenderCode.MALE);

        patientRegistryFeedClient = new PatientRegistryFeedClient();
        patientRegistryFeedClient.setClient(patientFeedClientMock);

        Mockito.doReturn(MALFORMED_UUID).when(patientFeedClientMock).createPatient(any());
        try {
            assertEquals(MALFORMED_UUID, patientRegistryFeedClient.createPatient(patient).getId());
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    @Description("Test on create, for particular exceptions returned from PatientFeedApplication")
    @Severity(SeverityLevel.CRITICAL)
    @Story("create")
    public void TestFeedThrowsPatientFeedCannotCrossRef() throws PatientFeedException {

        Patient patient = createPatient("name", "name", LocalDate.of(1990, 06, 19), GenderCode.MALE);

        PatientFeedProcessResponseException embedException = new PatientFeedProcessResponseException("Impossible to cross reference the patient (not saved)");
        PatientFeedException firstException = new PatientFeedException(embedException);

        patientRegistryFeedClient = new PatientRegistryFeedClient();
        patientRegistryFeedClient.setClient(patientFeedClientMock);

        Mockito.doThrow(firstException).when(patientFeedClientMock).createPatient(any());

        assertThrows(InternalErrorException.class, () -> patientRegistryFeedClient.createPatient(patient));
        try {
            patientRegistryFeedClient.createPatient(patient);
        } catch (InternalErrorException | ConversionException e) {
            assertEquals("Impossible to cross reference the patient, it will not be saved !", e.getMessage());
        }
    }

    @Test
    @Description("Test on create, for particular exceptions returned from PatientFeedApplication")
    @Severity(SeverityLevel.CRITICAL)
    @Story("create")
    public void TestFeedThrowsPatientFeedPersistingPatient() throws PatientFeedException {

        Patient patient = createPatient("name", "name", LocalDate.of(1990, 06, 19), GenderCode.MALE);

        PatientFeedProcessResponseException embedException = new PatientFeedProcessResponseException("Unexpected Exception persisting Patient !");
        PatientFeedException firstException = new PatientFeedException(embedException);

        patientRegistryFeedClient = new PatientRegistryFeedClient();
        patientRegistryFeedClient.setClient(patientFeedClientMock);

        Mockito.doThrow(firstException).when(patientFeedClientMock).createPatient(any());

        assertThrows(InternalErrorException.class, () -> patientRegistryFeedClient.createPatient(patient));
        try {
            patientRegistryFeedClient.createPatient(patient);
        } catch (InternalErrorException e) {
            assertEquals("Unexpected Exception persisting Patient !", e.getMessage());
        } catch (ConversionException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    @Description("Test on create, for particular exceptions returned from PatientFeedApplication")
    @Severity(SeverityLevel.CRITICAL)
    @Story("create")
    public void TestFeedThrowsPatientFeedExceptionSystemNotFound() throws PatientFeedException {

        Patient patient = createPatient("name", "name", LocalDate.of(1990, 06, 19), GenderCode.MALE);

        PatientFeedProcessResponseException embedException = new PatientFeedProcessResponseException("System not found");
        PatientFeedException firstException = new PatientFeedException("Exception while Mapping with GITB elements !", embedException);

        patientRegistryFeedClient = new PatientRegistryFeedClient();
        patientRegistryFeedClient.setClient(patientFeedClientMock);

        Mockito.doThrow(firstException).when(patientFeedClientMock).createPatient(any());

        assertThrows(InternalErrorException.class, () -> patientRegistryFeedClient.createPatient(patient));
        try {
            patientRegistryFeedClient.createPatient(patient);
        } catch (InternalErrorException | ConversionException e) {
            assertEquals("Exception while Mapping with GITB elements !", e.getMessage());
        }
    }

    @Test
    @Description("Test on update, exception thrown when no Patient is given")
    @Severity(SeverityLevel.CRITICAL)
    @Story("update")
    public void TestFeedUpdateNullPatient() throws PreferenceException, NamespaceException, PatientFeedException {

        String uuid = TEST_UUID;

        patientRegistryFeedClient = new PatientRegistryFeedClient();
        patientRegistryFeedClient.setClient(patientFeedClientMock);

        assertThrows(InvalidRequestException.class, () -> patientRegistryFeedClient.updatePatient(null, null));

    }


    @Test
    @Description("Test on update, exception thrown from PatReg")
    @Severity(SeverityLevel.CRITICAL)
    @Story("update")
    public void TestFeedUpdatePatientFeedInvalidOperationThrown() throws PreferenceException, NamespaceException, PatientFeedException {

        String uuid = TEST_UUID;
        Patient patient = createPatient("", "", LocalDate.of(1990, 06, 19), GenderCode.MALE);

        PatientFeedException embedException = new PatientFeedException("Invalid operation used on distant PatientFeedProcessingService !");

        patientRegistryFeedClient = new PatientRegistryFeedClient();
        patientRegistryFeedClient.setClient(patientFeedClientMock);

        Mockito.doThrow(embedException).when(patientFeedClientMock).updatePatient(any(), any());

        assertThrows(InternalErrorException.class, () -> patientRegistryFeedClient.updatePatient(patient, null));
        try {
            patientRegistryFeedClient.updatePatient(patient, null);
        } catch (InternalErrorException | ConversionException e) {
            assertEquals("Invalid operation used on distant PatientFeedProcessingService !", e.getMessage());
        }
    }

    @Test
    @Description("Test on update, exception thrown from PatReg")
    @Severity(SeverityLevel.CRITICAL)
    @Story("update")
    public void TestFeedUpdatePatientInvalidRequest() throws PatientFeedException {

        String uuid = TEST_UUID;
        Patient patient = createPatient("", "", LocalDate.of(1990, 06, 19), GenderCode.MALE);

        PatientFeedException embedException = new PatientFeedException("Invalid Request sent to distant PatientFeedProcessingService !");

        patientRegistryFeedClient = new PatientRegistryFeedClient();
        patientRegistryFeedClient.setClient(patientFeedClientMock);

        Mockito.doThrow(embedException).when(patientFeedClientMock).updatePatient(any(), any());

        assertThrows(InternalErrorException.class, () -> patientRegistryFeedClient.updatePatient(patient,  null));
        try {
            patientRegistryFeedClient.updatePatient(patient, null);
        } catch (InternalErrorException e) {
            assertEquals("Invalid Request sent to distant PatientFeedProcessingService !", e.getMessage());
        } catch (ConversionException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    @Description("Test on update, exception thrown from PatReg")
    @Severity(SeverityLevel.CRITICAL)
    @Story("update")
    public void TestFeedUpdatePatientUnhandled() throws PatientFeedException {

        String uuid = TEST_UUID;
        Patient patient = createPatient("", "", LocalDate.of(1990, 06, 19), GenderCode.MALE);

        PatientFeedException embedException = new PatientFeedException("");

        patientRegistryFeedClient = new PatientRegistryFeedClient();
        patientRegistryFeedClient.setClient(patientFeedClientMock);

        Mockito.doThrow(embedException).when(patientFeedClientMock).updatePatient(any(), any());

        assertThrows(InternalErrorException.class, () -> patientRegistryFeedClient.updatePatient(patient, null));
        try {
            patientRegistryFeedClient.updatePatient(patient, null);
        } catch (InternalErrorException | ConversionException e) {
            assertEquals("An unhandled error was thrown.", e.getMessage());
        }
    }

    @Test
    @Description("Test on merge, when feeding basic request")
    @Severity(SeverityLevel.CRITICAL)
    @Story("merge")
    public void TestFeedMergeNominalCase() throws PatientFeedException {

        EntityIdentifier identifier1 = new EntityIdentifier();
        EntityIdentifier identifier2 = new EntityIdentifier();

        patientRegistryFeedClient = new PatientRegistryFeedClient();
        patientRegistryFeedClient.setClient(patientFeedClientMock);

        Mockito.doReturn(TEST_UUID).when(patientFeedClientMock).mergePatient(any(), any());
        //TODO change expected value when feature will be done
        assertEquals(TEST_UUID, patientRegistryFeedClient.mergePatient("identifier1", "identifier2").getId());

    }

    @Test
    @Description("Test on merge, exception thrown when no Patient is given")
    @Severity(SeverityLevel.CRITICAL)
    @Story("merge")
    public void TestFeedMergeNullPatient() {

        EntityIdentifier identifier2 = new EntityIdentifier();

        patientRegistryFeedClient = new PatientRegistryFeedClient();
        patientRegistryFeedClient.setClient(patientFeedClientMock);

        assertThrows(InvalidRequestException.class, () -> patientRegistryFeedClient.mergePatient(null, null));

    }

    @Test
    @Description("Test on merge, exception thrown when no Uuid is given")
    @Severity(SeverityLevel.CRITICAL)
    @Story("merge")
    public void TestFeedMergeNullUuid() {
        patientRegistryFeedClient = new PatientRegistryFeedClient();
        patientRegistryFeedClient.setClient(patientFeedClientMock);

        assertThrows(InvalidRequestException.class, () -> patientRegistryFeedClient.mergePatient(null, null));

    }

    @Test
    @Description("Test on create, for particular exceptions returned from PatientFeedApplication")
    @Severity(SeverityLevel.CRITICAL)
    @Story("delete")
    public void TestFeedDelete() throws PatientFeedException {

        patientRegistryFeedClient = new PatientRegistryFeedClient();
        patientRegistryFeedClient.setClient(patientFeedClientMock);

        Mockito.doReturn(true).when(patientFeedClientMock).deletePatient(TEST_UUID);

        try {
            assertTrue(patientRegistryFeedClient.delete(TEST_UUID));
        } catch (InternalErrorException e) {
            fail("Deletion should not have thrown an error");
        }
    }

    @Test
    @Description("Test on create, for particular exceptions returned from PatientFeedApplication")
    @Severity(SeverityLevel.CRITICAL)
    @Story("delete")
    public void TestFeedDeleteBlankUUID() {

        patientRegistryFeedClient = new PatientRegistryFeedClient();
        patientRegistryFeedClient.setClient(patientFeedClientMock);

        assertThrows(InvalidRequestException.class, () -> patientRegistryFeedClient.delete(""));

        try {
            patientRegistryFeedClient.delete("");
        } catch (InvalidRequestException e) {
            assertEquals("Invalid parameter", e.getMessage());
        } catch (Exception e) {
            fail("Test failed, the expected result has not happend");
        }
    }

    @Test
    @Description("Test on create, for particular exceptions returned from PatientFeedApplication")
    @Severity(SeverityLevel.CRITICAL)
    @Story("delete")
    public void TestFeedDeleteStatusGone() throws PatientFeedException {

        patientRegistryFeedClient = new PatientRegistryFeedClient();
        patientRegistryFeedClient.setClient(patientFeedClientMock);

        Mockito.doReturn(false).when(patientFeedClientMock).deletePatient(TEST_UUID);
        assertThrows(ResourceGoneException.class, () -> patientRegistryFeedClient.delete(TEST_UUID));

        try {
            patientRegistryFeedClient.delete(TEST_UUID);
        } catch (ResourceGoneException e) {
            assertEquals("Patient with UUID " + TEST_UUID, e.getMessage());
        } catch (Exception e) {
            fail("Test failed, the expected result has not happend");
        }
    }

    @Test
    @Description("Test on delete, for a particular exception returned from PatientFeedApplication")
    @Severity(SeverityLevel.CRITICAL)
    @Story("delete")
    public void TestFeedDeleteNoUuid() throws PatientFeedException {

        PatientFeedProcessResponseException embedException = new PatientFeedProcessResponseException("The uuid cannot be null or empty");
        PatientFeedException firstException = new PatientFeedException(embedException);

        patientRegistryFeedClient = new PatientRegistryFeedClient();
        patientRegistryFeedClient.setClient(patientFeedClientMock);

        Mockito.doThrow(firstException).when(patientFeedClientMock).deletePatient(TEST_UUID);
        assertThrows(InternalErrorException.class, () -> patientRegistryFeedClient.delete(TEST_UUID));

        try {
            patientRegistryFeedClient.delete(TEST_UUID);
        } catch (InternalErrorException e) {
            assertEquals("The uuid cannot be null or empty", e.getMessage());
        } catch (Exception e) {
            fail("Test failed, the expected result has not happend");
        }
    }

    @Test
    @Description("Test on delete, for a particular exception returned from PatientFeedApplication")
    @Severity(SeverityLevel.CRITICAL)
    @Story("delete")
    public void TestFeedDeleteDeleteCannotOperate() throws PatientFeedException {

        PatientFeedProcessResponseException embedException = new PatientFeedProcessResponseException("Cannot proceed to delete");
        PatientFeedException firstException = new PatientFeedException(embedException);

        patientRegistryFeedClient = new PatientRegistryFeedClient();
        patientRegistryFeedClient.setClient(patientFeedClientMock);

        Mockito.doThrow(firstException).when(patientFeedClientMock).deletePatient(TEST_UUID);
        assertThrows(InternalErrorException.class, () -> patientRegistryFeedClient.delete(TEST_UUID));

        try {
            patientRegistryFeedClient.delete(TEST_UUID);
        } catch (InternalErrorException e) {
            assertEquals("Cannot proceed to delete", e.getMessage());
        } catch (Exception e) {
            fail("Test failed, the expected result has not happend");
        }
    }

    @Test
    @Description("Test on delete, for a particular exception returned from PatientFeedApplication")
    @Severity(SeverityLevel.CRITICAL)
    @Story("delete")
    public void TestFeedDeleteDeleteInvalidResponse() throws PatientFeedException {

        PatientFeedProcessResponseException embedException = new PatientFeedProcessResponseException("other error");
        PatientFeedException firstException = new PatientFeedException("Invalid Response from distant PatientFeedProcessingService !", embedException);

        patientRegistryFeedClient = new PatientRegistryFeedClient();
        patientRegistryFeedClient.setClient(patientFeedClientMock);

        Mockito.doThrow(firstException).when(patientFeedClientMock).deletePatient(TEST_UUID);
        assertThrows(InternalErrorException.class, () -> patientRegistryFeedClient.delete(TEST_UUID));

        try {
            patientRegistryFeedClient.delete(TEST_UUID);
        } catch (InternalErrorException e) {
            assertEquals("Invalid Response from distant PatientFeedProcessingService !", e.getMessage());
        } catch (Exception e) {
            fail("Test failed, the expected result has not happend");
        }
    }

    private Patient createPatient(String familyName, String givenName, LocalDate birthDate, GenderCode gender) {

        Patient patient = new Patient();

        PersonName name = new PersonName();
        name.addGiven(givenName);
        name.setFamily(familyName);
        patient.addName(name);
        patient.setDateOfBirth(Date.from(birthDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
        patient.setGender(gender);
        return patient;

    }

}
