package net.ihe.gazelle.application.mock;

import com.gitb.ps.ProcessingService;
import net.ihe.gazelle.app.patientregistryapi.application.SearchCrossReferenceException;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryapi.business.PatientAliases;
import net.ihe.gazelle.app.patientregistryxrefsearchclient.adapter.XRefSearchClient;

import java.util.List;

public class XRefSearchClientMock extends XRefSearchClient {

    public XRefSearchClientMock(ProcessingService processingService) {
        super(processingService);
    }

    @Override
    public PatientAliases search(EntityIdentifier sourceIdentifier, List<String> targetDomains) throws SearchCrossReferenceException {
        PatientAliases patientAliases = new PatientAliases();
        switch (sourceIdentifier.getSystemIdentifier()) {
            case "":
                return patientAliases;
            default:
                return null;
        }
    }
}
