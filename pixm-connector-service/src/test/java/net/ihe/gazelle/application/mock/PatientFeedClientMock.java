package net.ihe.gazelle.application.mock;

import com.gitb.ps.ProcessingService;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryfeedclient.adapter.PatientFeedClient;

public class PatientFeedClientMock extends PatientFeedClient {

    public PatientFeedClientMock(ProcessingService processingService) {
        super(processingService);
    }

    @Override
    public String createPatient(Patient patient) {

        return null;

    }

    @Override
    public boolean deletePatient(String uuid) {

        return false;

    }

}
