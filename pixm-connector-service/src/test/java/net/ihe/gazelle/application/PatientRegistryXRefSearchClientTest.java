package net.ihe.gazelle.application;

import ca.uhn.fhir.rest.server.exceptions.ForbiddenOperationException;
import ca.uhn.fhir.rest.server.exceptions.InternalErrorException;
import ca.uhn.fhir.rest.server.exceptions.InvalidRequestException;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import net.ihe.gazelle.app.patientregistryapi.application.SearchCrossReferenceException;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryapi.business.PatientAliases;
import net.ihe.gazelle.app.patientregistryapi.business.PersonName;
import net.ihe.gazelle.app.patientregistryxrefsearchclient.adapter.XRefSearchClient;
import net.ihe.gazelle.framework.preferencesmodelapi.application.NamespaceException;
import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesService;
import net.ihe.gazelle.framework.preferencesmodelapi.application.PreferenceException;
import org.hl7.fhir.r4.model.Parameters;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import jakarta.xml.ws.WebServiceException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class PatientRegistryXRefSearchClientTest {
    @Mock
    static private XRefSearchClient xRefSearchClientMock;
    @Mock
    static private OperationalPreferencesService operationalPreferencesService;
    @Mock
    private PatientRegistryXRefSearchClient patientRegistryXRefSearchClient;

    @BeforeAll
    static void initialize() {
        xRefSearchClientMock = Mockito.mock(XRefSearchClient.class);
        operationalPreferencesService = Mockito.mock(OperationalPreferencesService.class);
    }

    @NotNull
    private static PatientAliases getPatientAliases() {
        PatientAliases patientAliases = new PatientAliases();
        Patient patient = new Patient();
        patient.setUuid("test1");
        PersonName personName = new PersonName();
        personName.setFamily("Test");
        patient.addName(personName);
        EntityIdentifier sourceIdentifier2 = new EntityIdentifier();
        sourceIdentifier2.setSystemIdentifier("domain1");
        sourceIdentifier2.setValue("1");
        patient.addIdentifier(sourceIdentifier2);
        patientAliases.addMember(patient);
        return patientAliases;
    }

    @Test
    @Description("Test on initialization, when a namespace exception is thrown")
    @Severity(SeverityLevel.CRITICAL)
    @Story("initialization")
    @Disabled
    void TestInitializeNameSpaceException() throws PreferenceException, NamespaceException {
        Mockito.doThrow(NamespaceException.class).when(operationalPreferencesService).getStringValue("java:/app/gazelle/pixm-connector" +
                "/operational" +
                "-preferences", "xrefpatientregistry.url");
        patientRegistryXRefSearchClient = new PatientRegistryXRefSearchClient(operationalPreferencesService);

        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("Test1");
        sourceIdentifier.setValue("1");
        List<String> targetDomains = new ArrayList<>();
        assertThrows(SearchCrossReferenceException.class,
                () -> patientRegistryXRefSearchClient.process(sourceIdentifier, targetDomains));
    }

    @Test
    @Description("Test on initialization, when a preference exception is thrown")
    @Severity(SeverityLevel.CRITICAL)
    @Story("initialization")
    @Disabled
    void TestInitializePreferenceException() throws PreferenceException, NamespaceException {
        Mockito.doThrow(PreferenceException.class).when(operationalPreferencesService).getStringValue("java:/app/gazelle/pixm-connector" +
                "/operational" +
                "-preferences", "xrefpatientregistry.url");
        patientRegistryXRefSearchClient = new PatientRegistryXRefSearchClient(operationalPreferencesService);

        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("Test1");
        sourceIdentifier.setValue("1");
        List<String> targetDomains = new ArrayList<>();
        assertThrows(SearchCrossReferenceException.class,
                () -> patientRegistryXRefSearchClient.process(sourceIdentifier, targetDomains));
    }

    @Test
    @Description("Test on initialization, when a malformed url exception is thrown")
    @Severity(SeverityLevel.CRITICAL)
    @Story("initialization")
    @Disabled
    void TestInitializeMalformedURLException() throws PreferenceException, NamespaceException {
        Mockito.doThrow(PreferenceException.class).when(operationalPreferencesService).getStringValue("java:/app/gazelle/pixm-connector" +
                "/operational" +
                "-preferences", "xrefpatientregistry.url");
        patientRegistryXRefSearchClient = new PatientRegistryXRefSearchClient(operationalPreferencesService);

        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("Test1");
        sourceIdentifier.setValue("1");
        List<String> targetDomains = new ArrayList<>();
        assertThrows(SearchCrossReferenceException.class,
                () -> patientRegistryXRefSearchClient.process(sourceIdentifier, targetDomains));
    }

    @Test
    @Description("Test on initialization, when a web service exception is thrown")
    @Severity(SeverityLevel.CRITICAL)
    @Story("initialization")
    @Disabled
    void TestInitializeWebServiceException() throws PreferenceException, NamespaceException {
        Mockito.doThrow(WebServiceException.class).when(operationalPreferencesService).getStringValue("java:/app/gazelle/pixm-connector" +
                "/operational" +
                "-preferences", "xrefpatientregistry.url");
        patientRegistryXRefSearchClient = new PatientRegistryXRefSearchClient(operationalPreferencesService);

        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("Test1");
        sourceIdentifier.setValue("1");
        List<String> targetDomains = new ArrayList<>();
        assertThrows(SearchCrossReferenceException.class,
                () -> patientRegistryXRefSearchClient.process(sourceIdentifier, targetDomains));
    }

    @Test
    @Description("Test on ihe_pix, a specific excpetion is thrown")
    @Severity(SeverityLevel.CRITICAL)
    @Story("ihe_pix")
    void TestSearchCrossExceptionInvalidRequest() throws SearchCrossReferenceException {
        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("Test1");
        sourceIdentifier.setValue("1");
        List<String> targetDomains = new ArrayList<>();
        SearchCrossReferenceException exception = new SearchCrossReferenceException("Error in the sourceIdentifier : System does not exit");
        Mockito.doThrow(new SearchCrossReferenceException(exception)).when(xRefSearchClientMock).search(sourceIdentifier, targetDomains);
        patientRegistryXRefSearchClient = new PatientRegistryXRefSearchClient();
        patientRegistryXRefSearchClient.setClient(xRefSearchClientMock);
        assertThrows(InvalidRequestException.class,
                () -> patientRegistryXRefSearchClient.process(sourceIdentifier, targetDomains));
    }

    @Test
    @Description("Test on ihe_pix, a specific excpetion is thrown")
    @Severity(SeverityLevel.CRITICAL)
    @Story("ihe_pix")
    void TestSearchCrossExceptionResourceNotFound() throws SearchCrossReferenceException {
        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("Test1");
        sourceIdentifier.setValue("1");
        List<String> targetDomains = new ArrayList<>();
        SearchCrossReferenceException exception = new SearchCrossReferenceException("Error in the sourceIdentifier : it does not match any Patient");
        Mockito.doThrow(new SearchCrossReferenceException(exception)).when(xRefSearchClientMock).search(sourceIdentifier, targetDomains);
        patientRegistryXRefSearchClient = new PatientRegistryXRefSearchClient();
        patientRegistryXRefSearchClient.setClient(xRefSearchClientMock);
        assertThrows(ResourceNotFoundException.class,
                () -> patientRegistryXRefSearchClient.process(sourceIdentifier, targetDomains));
    }

    @Test
    @Description("Test on ihe_pix, a specific excpetion is thrown")
    @Severity(SeverityLevel.CRITICAL)
    @Story("ihe_pix")
    void TestSearchCrossExceptionForbiddenOperation() throws SearchCrossReferenceException {
        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("Test1");
        sourceIdentifier.setValue("1");
        List<String> targetDomains = new ArrayList<>();
        SearchCrossReferenceException exception = new SearchCrossReferenceException("One of the target domain does not exist");
        Mockito.doThrow(new SearchCrossReferenceException(exception)).when(xRefSearchClientMock).search(sourceIdentifier, targetDomains);
        patientRegistryXRefSearchClient = new PatientRegistryXRefSearchClient();
        patientRegistryXRefSearchClient.setClient(xRefSearchClientMock);
        assertThrows(ForbiddenOperationException.class,
                () -> patientRegistryXRefSearchClient.process(sourceIdentifier, targetDomains));
    }

    @Test
    @Description("Test on ihe_pix, a specific excpetion is thrown")
    @Severity(SeverityLevel.CRITICAL)
    @Story("ihe_pix")
    void TestSearchCrossExceptionInternalError() throws SearchCrossReferenceException {
        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("Test1");
        sourceIdentifier.setValue("1");
        List<String> targetDomains = new ArrayList<>();
        SearchCrossReferenceException exception = new SearchCrossReferenceException("Test");
        Mockito.doThrow(new SearchCrossReferenceException(exception)).when(xRefSearchClientMock).search(sourceIdentifier, targetDomains);
        patientRegistryXRefSearchClient = new PatientRegistryXRefSearchClient();
        patientRegistryXRefSearchClient.setClient(xRefSearchClientMock);
        assertThrows(InternalErrorException.class,
                () -> patientRegistryXRefSearchClient.process(sourceIdentifier, targetDomains));
    }

    @Test
    @Description("Test on ihe_pix, when no Patient is returned")
    @Severity(SeverityLevel.CRITICAL)
    @Story("ihe_pix")
    void TestSearchCrossNoMember() throws SearchCrossReferenceException {
        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("Test1");
        sourceIdentifier.setValue("1");
        List<String> targetDomains = new ArrayList<>();
        PatientAliases patientAliases = new PatientAliases();
        Mockito.doReturn(patientAliases).when(xRefSearchClientMock).search(sourceIdentifier, targetDomains);
        patientRegistryXRefSearchClient = new PatientRegistryXRefSearchClient();
        patientRegistryXRefSearchClient.setClient(xRefSearchClientMock);
        assertThrows(ResourceNotFoundException.class,
                () -> patientRegistryXRefSearchClient.process(sourceIdentifier, targetDomains));
    }

    @Test
    @Description("Test on ihe_pix, when one Patient is returned")
    @Severity(SeverityLevel.CRITICAL)
    @Story("ihe_pix")
    void TestSearchCross1Member() throws SearchCrossReferenceException {
        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("Test1");
        sourceIdentifier.setValue("1");
        List<String> targetDomains = new ArrayList<>();
        targetDomains.add("domain1");
        PatientAliases patientAliases = getPatientAliases();
        Mockito.doReturn(patientAliases).when(xRefSearchClientMock).search(sourceIdentifier, targetDomains);
        patientRegistryXRefSearchClient = new PatientRegistryXRefSearchClient();
        patientRegistryXRefSearchClient.setClient(xRefSearchClientMock);
        Parameters parameters = patientRegistryXRefSearchClient.process(sourceIdentifier, targetDomains);
        assertEquals(2, parameters.getParameter().size());
    }

    @Test
    @Description("Test on ihe_pix, when one target domain was given as parameter")
    @Severity(SeverityLevel.CRITICAL)
    @Story("ihe_pix")
    void TestSearchNotargetDomain() throws SearchCrossReferenceException {
        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("Test1");
        sourceIdentifier.setValue("1");
        List<String> targetDomains = new ArrayList<>();
        targetDomains.add("domain3");
        PatientAliases patientAliases = getPatientAliases();
        Mockito.doReturn(patientAliases).when(xRefSearchClientMock).search(sourceIdentifier, targetDomains);
        patientRegistryXRefSearchClient = new PatientRegistryXRefSearchClient();
        patientRegistryXRefSearchClient.setClient(xRefSearchClientMock);
        Parameters parameters = patientRegistryXRefSearchClient.process(sourceIdentifier, targetDomains);
        assertEquals(1, parameters.getParameter().size());
    }

    @Description("Test on ihe_pix, when two target domain was given as parameter")
    @Severity(SeverityLevel.CRITICAL)
    @Story("ihe_pix")
    @Test
    void TestSearchfullTargetDomain() throws SearchCrossReferenceException {
        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("Test1");
        sourceIdentifier.setValue("1");
        List<String> targetDomains = new ArrayList<>();
        PatientAliases patientAliases = new PatientAliases();
        Patient patient = new Patient();
        patient.setUuid("test1");
        PersonName personName = new PersonName();
        personName.setFamily("Test");
        patient.addName(personName);
        EntityIdentifier sourceIdentifier2 = new EntityIdentifier();
        sourceIdentifier2.setSystemIdentifier("domain1");
        sourceIdentifier2.setValue("1");
        EntityIdentifier sourceIdentifier3 = new EntityIdentifier();
        sourceIdentifier3.setSystemIdentifier("domain1");
        sourceIdentifier3.setValue("1");
        patient.addIdentifier(sourceIdentifier2);
        patient.addIdentifier(sourceIdentifier3);
        patientAliases.addMember(patient);
        Mockito.doReturn(patientAliases).when(xRefSearchClientMock).search(sourceIdentifier, targetDomains);
        patientRegistryXRefSearchClient = new PatientRegistryXRefSearchClient();
        patientRegistryXRefSearchClient.setClient(xRefSearchClientMock);
        Parameters parameters = patientRegistryXRefSearchClient.process(sourceIdentifier, targetDomains);
        assertEquals(3, parameters.getParameter().size());
    }
}