package net.ihe.gazelle.application.mock;

import com.gitb.ps.ProcessingService;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryapi.business.PatientAliases;
import net.ihe.gazelle.app.patientregistrysearchclient.adapter.PatientSearchClient;
import net.ihe.gazelle.lib.searchmodelapi.business.SearchCriteria;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.SearchCriterion;

import java.util.List;

public class SearchClientMock extends PatientSearchClient {

    public SearchClientMock(ProcessingService processingService) {
        super(processingService);
    }

    @Override
    public List<Patient> search(SearchCriteria searchCriteria) throws SearchException {
        PatientAliases patientAliases = new PatientAliases();
        for (SearchCriterion criterion : searchCriteria.getSearchCriterions()) {
            switch (criterion.toString()) {
                case "":
                    return null;
                default:
                    return null;
            }
        }
        return null;
    }

}
