package net.ihe.gazelle.business.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.param.*;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.exceptions.InvalidRequestException;
import ca.uhn.fhir.rest.server.exceptions.UnprocessableEntityException;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.servlet.http.HttpServletRequest;
import net.ihe.gazelle.adapter.connector.ConversionException;
import net.ihe.gazelle.adapter.connector.FhirToGazelleRegistryConverter;
import net.ihe.gazelle.app.patientregistryapi.application.PatientFeedException;
import net.ihe.gazelle.app.patientregistryapi.application.SearchCrossReferenceException;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.application.ConfigurationAdapter;
import net.ihe.gazelle.application.PatientRegistryFeedClient;
import net.ihe.gazelle.application.PatientRegistrySearchClient;
import net.ihe.gazelle.application.PatientRegistryXRefSearchClient;
import net.ihe.gazelle.business.service.RequestValidatorService;
import net.ihe.gazelle.lib.annotations.Package;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationReport;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationTestResult;
import net.ihe.gazelle.validation.api.domain.request.structure.ValidationItem;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * This is a resource provider which stores Patient resources in memory using a HashMap. This is obviously not a production-ready solution for many
 * reasons,
 * but it is useful to help illustrate how to build a fully-functional server.
 */
@Named("ihePatientResourceProvider")
public class IhePatientResourceProvider implements IResourceProvider {

    public static final String SOURCE_IDENTIFIER_PATIENT_IDENTIFIER_NOT_FOUND = "sourceIdentifier or Patient Identifier not found";
    public static final String TARGET_SYSTEM_NOT_FOUND = "targetSystem not found";
    public static final String NO_IDENTIFIER_PROVIDED = "No Identifier provided";
    public static final String NO_PATIENT_PROVIDED = "No Patient Provided";
    public static final String PROBLEM_DURING_VALIDATION = "Problem during Validation of Pixm Patient: ";
    public static final String SOURCE_IDENTIFIER = "sourceIdentifier";
    public static final String TARGET_SYSTEM = "targetSystem";
    public static final String FHIR_PATIENT_COULD_NOT_BE_CONVERTED_TO_REGISTRY_PATIENT = "Patient Could not be converted to HL7 Patient";
    public static final String PROBLEM_DURING_CREATING_PATIENT_IN_PATIENT_REGISTRY = "Problem during creating patient in Patient registry";
    public static final String ERROR_IN_DELETION = "Error in deletion";
    public static final String PATIENT_FEED_CLIENT_IS_NOT_SET = "Patient Feed client is not set";
    public static final String PATIENT_SUCCESSFULLY_FOUND = "Patient Successfully found";
    public static final String PATIENT_COULD_NOT_BE_RETRIEVED = "Patient could not be retrieved";
    private static final Logger patientLogger = LoggerFactory.getLogger(IhePatientResourceProvider.class);
    private static final String URN_OID = "urn:oid:";
    @Inject
    public RequestValidatorService requestValidatorService;
    @Inject
    protected PatientRegistryXRefSearchClient patientRegistryXRefSearchClient;
    @Inject
    protected PatientRegistrySearchClient patientRegistrySearchClient;
    @Inject
    protected PatientRegistryFeedClient patientRegistryFeedClient;

    @Inject
    protected ConfigurationAdapter configurationAdapter;



    private IhePatientResourceProvider() {
    }

    @Package
    public IhePatientResourceProvider(PatientRegistryXRefSearchClient xRefClient,
                                      PatientRegistrySearchClient client,
                                      PatientRegistryFeedClient patientFeedClient,
                                      RequestValidatorService requestValidatorService,
                                      ConfigurationAdapter configurationAdapter) {
        this.requestValidatorService = requestValidatorService;
        this.patientRegistryXRefSearchClient = xRefClient;
        this.patientRegistrySearchClient = client;
        this.configurationAdapter = configurationAdapter;
        this.patientRegistryFeedClient = patientFeedClient;
    }

    /**
     * The getResourceType method comes from IResourceProvider, and must be overridden to indicate what type of resource this provider supplies.
     */
    @Override
    public Class<? extends IBaseResource> getResourceType() {
        return Patient.class;
    }

    /**
     * Method called to create a Patient on Patient Registry
     *
     * @param iti104Patient : the Patient to create.
     * @return a MethodOutcome containing a representation of the patient created.
     */
    @Create
    public MethodOutcome create(@ResourceParam Patient iti104Patient, HttpServletRequest request) throws UnprocessableEntityException {
        checkIfPatientExist(iti104Patient);
        String profileId = configurationAdapter.getProfileIdCreateUpdateDeleteIti104();
        validateInputs(request, iti104Patient, profileId);
        return addNewPatientIntoRegistry(iti104Patient);
    }

    /**
     * Method to Update a patient related to PIXm Delete Method
     *
     * @param theConditional         the identifier of the Patient we want to update
     * @param iti104Patient The Bundle content of the patient
     * @return FhirBundle that contains the updated patient
     */
    @Update
    public MethodOutcome update(@IdParam IdType theId, @ConditionalUrlParam String theConditional, @ResourceParam Patient iti104Patient, HttpServletRequest request) {
        checkIfPatientExist(iti104Patient);
        checkIfIdIsPresent(theConditional, iti104Patient);


        validateInputs(request, iti104Patient, configurationAdapter.getProfileIdCreateUpdateDeleteIti104());
        // the '?' is surely not at the beginning of the string
        EntityIdentifier identifier = createEntityIdentifierFromConditional(theConditional);
        if(identifier == null){
            throw new UnprocessableEntityException(NO_IDENTIFIER_PROVIDED);
        }
        try {
            Patient patientUpdated;
            if(iti104Patient.hasActive() && !iti104Patient.getActive()){ // this forces the true as a default value
                patientUpdated = patientRegistryFeedClient.deactivatePatient(iti104Patient, identifier);
            }
            else{
                patientUpdated = patientRegistryFeedClient.updatePatient(FhirToGazelleRegistryConverter.convertPatient(iti104Patient), identifier);
            }

            MethodOutcome methodOutcome = new MethodOutcome();
            methodOutcome.setResource(patientUpdated);
            return methodOutcome;
        } catch (ConversionException e) {
            throw new UnprocessableEntityException(FHIR_PATIENT_COULD_NOT_BE_CONVERTED_TO_REGISTRY_PATIENT, e);
        } catch (PatientFeedException e) {
            throw new UnprocessableEntityException(PATIENT_FEED_CLIENT_IS_NOT_SET);
        }

    }

    /**
     * Method to delete a Patient related to PIXm Delete Method
     *
     * @param theConditional of the patient to delete
     * @return FhirBundle that contains the deletion status
     */
    @Delete
    public MethodOutcome delete(@IdParam IdType theId, @ConditionalUrlParam (supportsMultiple = true) String theConditional, HttpServletRequest request) {

        if (theConditional == null || theConditional.isEmpty()) {
            throw new UnprocessableEntityException(NO_IDENTIFIER_PROVIDED);
        }

        validateInputs(request, null, configurationAdapter.getProfileIdCreateUpdateDeleteIti104());

        try {
            EntityIdentifier identifier = createEntityIdentifierFromConditional(theConditional);
            EntityIdentifier deleted = patientRegistryFeedClient.delete(identifier);
            MethodOutcome methodOutcome = new MethodOutcome();
            int responseStatusCode = deleted == null || deleted.getValue() == null || deleted.getValue().isBlank()
                    ? 204
                    : 200;
            methodOutcome.setResponseStatusCode(responseStatusCode);
            return methodOutcome;
        } catch (Exception e) {
            throw new UnprocessableEntityException(ERROR_IN_DELETION, e);
        }
    }


    /**
     * Method to read a Patient through its uuid in the patient Manager database
     *
     * @param theId Id Type given in the url. represents the uuid of the searched patient
     * @return One patient corresponding to the uuid given in entry. If many Patients are found, we consider it as an error and won't return ANY
     * patient.
     */
    @Read
    public Patient read(@IdParam IdType theId) {
        String uuid = theId.getIdPart();
        if (uuid == null || uuid.isBlank()) {
            patientLogger.error(NO_IDENTIFIER_PROVIDED);
            throw new UnprocessableEntityException(NO_IDENTIFIER_PROVIDED);
        }
        try {
            Patient retrievedPatient = patientRegistrySearchClient.searchPatient(uuid);
            patientLogger.info(PATIENT_SUCCESSFULLY_FOUND);
            return retrievedPatient;
        } catch (SearchException e) {
            throw new UnprocessableEntityException(PATIENT_COULD_NOT_BE_RETRIEVED, e);
        }
    }

    /**
     * Search method for a Patient using the source identifier required parameter
     * and an optional list of target system
     *
     * @param sourceIdentifierParam : the source identifier of the patient, should be formatted "urn:oid:x.x.x.x.x.x.x.x.x.x|value"
     * @param targetSystemParam:    the target System(s) we want to find the Patient on, should be formatted "urn:oid:x.x.x.x.x.x.x.x.x.x,
     *                              urn:oid:x.x.x.x.x.x.x.x.x.x"
     * @return a Parameters element composed of a list of target identifier for every Patient found, and an url to the Patient in the server.
     */
    @Operation(name = "$ihe-pix", idempotent = true)
    public Parameters findCorrespondingIdentifiersWithGet(@OperationParam(name = SOURCE_IDENTIFIER) TokenAndListParam sourceIdentifierParam,
                                               @OperationParam(name = TARGET_SYSTEM) StringAndListParam targetSystemParam, HttpServletRequest request) {

        validateInputs(request, null, configurationAdapter.getProfileIdGetIti83());

        EntityIdentifier sourceIdentifier = createEntityIdentifierFromSourceIdentifier(sourceIdentifierParam);
        List<String> targetSystemList = createTargetSystemListFromParam(targetSystemParam);
        return getParametersFromIdentifierAndTarget(sourceIdentifier, targetSystemList);

    }

    //--------------------Private/protected methods-------------------//

    private Parameters getParametersFromIdentifierAndTarget(EntityIdentifier sourceIdentifier, List<String> targetSystemList) {
        Parameters parametersResults;
        try {
            parametersResults = patientRegistryXRefSearchClient.process(sourceIdentifier, targetSystemList);
        } catch (SearchCrossReferenceException | UnprocessableEntityException e) {
            throw new UnprocessableEntityException(e.getMessage());
        }
        return parametersResults;
    }

    private EntityIdentifier createEntityIdentifierFromSourceIdentifier(TokenAndListParam sourceIdentifier) {
        if (sourceIdentifier == null || sourceIdentifier.size() == 0) {
            throw new UnprocessableEntityException(SOURCE_IDENTIFIER_PATIENT_IDENTIFIER_NOT_FOUND);
        }
        TokenOrListParam tokenOrListParams = sourceIdentifier.getValuesAsQueryTokens().get(0);
        TokenParam source = tokenOrListParams.getValuesAsQueryTokens().get(0);
        String sourceIdentifierSystem = source.getSystem();
        String sourceIdentifierValue = source.getValue();
        return FhirToGazelleRegistryConverter.convertSourceIdentiferToEntityIdentifier(sourceIdentifierSystem, sourceIdentifierValue);
    }

    private EntityIdentifier createEntityIdentifierFromConditional(String conditional){
        String identifier = conditional.contains("=") ? conditional.substring(conditional.indexOf("=")+1) : null;
        //clear urn:oid: if present (need to be improved so this action handled by the UI)
        if(identifier != null) {
            identifier = identifier.replace(URN_OID, "");
        }
        else{
            return null;
        }
        String[] valueAndSystem = identifier.split("%7C");
        if(valueAndSystem.length != 2){
            throw new UnprocessableEntityException("Identifier does not respect the format : value|system");
        }
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setValue(valueAndSystem[1]);
        entityIdentifier.setSystemIdentifier(valueAndSystem[0]);
        return entityIdentifier;
    }


    private List<String> createTargetSystemListFromParam(StringAndListParam targetSystemParam) {
        List<String> targetSystemList = new ArrayList<>();
        if (targetSystemParam != null && targetSystemParam.size() > 0) {
            for (StringOrListParam listParam : targetSystemParam.getValuesAsQueryTokens()) {
                List<StringParam> queryStrings = listParam.getValuesAsQueryTokens();
                buildTargetSystemList(queryStrings, targetSystemList);
            }
        }

        return targetSystemList;
    }

    private void buildTargetSystemList(List<StringParam> list, List<String> targetSystemList) {
        for (StringParam singleParam : list) {
            String singleParamValue = singleParam.getValue();
            if (singleParamValue.contains(URN_OID)) {
                singleParamValue = singleParamValue.replace(URN_OID, "");
            }
            targetSystemList.add(singleParamValue);
        }
    }


    private void checkIfPatientExist(Patient iti104Patient) {
        if (iti104Patient == null) {
            patientLogger.error(NO_PATIENT_PROVIDED);
            throw new UnprocessableEntityException(NO_PATIENT_PROVIDED);
        }
    }

    private void checkIfIdIsPresent(String theId, Patient iti104Patient) {
        if (theId == null || theId.isBlank() || !iti104Patient.hasIdentifier()) {
            patientLogger.error(NO_IDENTIFIER_PROVIDED);
            throw new UnprocessableEntityException(NO_IDENTIFIER_PROVIDED);
        }
    }

    private MethodOutcome addNewPatientIntoRegistry(Patient iti104Patient) {
        try {
            Patient newPatient = patientRegistryFeedClient.createPatient(FhirToGazelleRegistryConverter.convertPatient(iti104Patient));
            if (newPatient != null) {
                MethodOutcome methodOutcome = new MethodOutcome();
                methodOutcome.setResource(newPatient);
                methodOutcome.setCreated(true);
                return methodOutcome;
            } else {
                throw new UnprocessableEntityException(PROBLEM_DURING_CREATING_PATIENT_IN_PATIENT_REGISTRY);
            }
        } catch (ConversionException e) {
            throw new UnprocessableEntityException(FHIR_PATIENT_COULD_NOT_BE_CONVERTED_TO_REGISTRY_PATIENT);
        } catch (PatientFeedException e) {
            throw new UnprocessableEntityException(PATIENT_FEED_CLIENT_IS_NOT_SET);
        }
    }

    private OperationOutcome getOperationOutcome(ValidationReport report) {
        OperationOutcome oo = new OperationOutcome();
        for (ValidationItem item : report.getValidationItems()) {
            if ("matchbox-validation".equals(item.getItemId())) {
                String returnedMessage = new String(item.getContent());
                oo = createFhirResourceFromString(OperationOutcome.class, returnedMessage);
            }
            if ("http-validation".equals(item.getItemId())) {
                OperationOutcome.OperationOutcomeIssueComponent issue = new OperationOutcome.OperationOutcomeIssueComponent();
                issue.setSeverity(OperationOutcome.IssueSeverity.ERROR);
                issue.setCode(OperationOutcome.IssueType.PROCESSING);
                issue.setDiagnostics(new String(item.getContent()));
                oo.addIssue(issue);
            }
        }
        return oo;
    }

    protected <T extends IBaseResource> T createFhirResourceFromString(Class<T> tClass, String input) {
        FhirContext ctx = FhirContext.forR4();
        IParser parser = ctx.newJsonParser();
        parser.setPrettyPrint(true);
        return parser.parseResource(tClass, input);
    }

    private void validateInputs(HttpServletRequest request, Resource resource, String profileId) throws InvalidRequestException {
        List<ValidationReport> validationReports = requestValidatorService.validateRequest(request, resource, profileId);
        for (ValidationReport vr : validationReports) {
            if (!ValidationTestResult.PASSED.equals(vr.getOverallResult())) {
                OperationOutcome oo = getOperationOutcome(vr);
                throw new InvalidRequestException(PROBLEM_DURING_VALIDATION, oo);
            }
        }
    }
}
