package net.ihe.gazelle.adapter.preferences;

import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesClientApplication;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OperationalPreferencesPIXm implements OperationalPreferencesClientApplication {
    @Override
    public Map<String, List<String>> wantedMandatoryPreferences() {
        Map<String, List<String>> mandatoryPreferences = new HashMap<>();
        List<String> deploymentPreferences = new ArrayList<>();

        deploymentPreferences.add(Preferences.XREF_PATREG.toString());
        deploymentPreferences.add(Preferences.PATIENT_REGISTRY_URL.toString());

        mandatoryPreferences.put(Namespaces.DEPLOYMENT.getValue(), deploymentPreferences);
        return mandatoryPreferences;
    }
}
