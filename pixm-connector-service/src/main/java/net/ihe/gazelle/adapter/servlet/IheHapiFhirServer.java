package net.ihe.gazelle.adapter.servlet;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.RestfulServer;
import net.ihe.gazelle.business.provider.IhePatientResourceProvider;

import jakarta.inject.Inject;
import jakarta.servlet.annotation.WebServlet;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;


@WebServlet(urlPatterns = {"/fhir/*"}, displayName = "FHIR Server IHE")
public class IheHapiFhirServer extends RestfulServer {
    @Inject
    private IhePatientResourceProvider ihePatientResourceProvider;
    @Serial
    private static final long serialVersionUID = 1L;
    public IheHapiFhirServer() {
        super(FhirContext.forR4());
    }

    /**
     * This method is called automatically when the
     * servlet is initializing.
     */
    @Override
    public void initialize() {
        List<IResourceProvider> providers = new ArrayList<>();
        providers.add(ihePatientResourceProvider);
        setResourceProviders(providers);

    }
}
