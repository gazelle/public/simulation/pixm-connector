package net.ihe.gazelle.adapter.preferences;

public enum Preferences {
    XREF_PATREG(Namespaces.DEPLOYMENT, "xrefpatientregistry.url"),
    PATIENT_REGISTRY_URL(Namespaces.DEPLOYMENT, "patientregistry.url");

    private final Namespaces namespace;
    private final String key;

    /**
     * Default constructor for the class. Defines the value of the Preference's key. It can then be used throughout the application to retrieve the
     * preference value.
     *
     * @param namespace value of the namespace where the Preference is defined.
     * @param key       value of the Preference's key.
     */
    Preferences(Namespaces namespace, String key) {
        this.namespace = namespace;
        this.key = key;
    }

    /**
     * Getter for the namespace property.
     *
     * @return the value of the namespace property.
     */
    public Namespaces getNamespace() {
        return namespace;
    }

    /**
     * Getter for the key property.
     *
     * @return the value of the key property.
     */
    public String getKey() {
        return key;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return key;
    }
}
