package net.ihe.gazelle.interlay.profiles;

import net.ihe.gazelle.application.ProfilesValidators;
import net.ihe.gazelle.application.ProfilesValidatorsFactory;

public class ITI83GetPIXmQueryProfileFactory implements ProfilesValidatorsFactory {

    public static final String PROFILE_ID_GET_ITI_83 = "PROFILE_ID_GET_ITI_83";

    @Override
    public ProfilesValidators createProfileValidator() {
        return new ITI83GetPIXmQueryProfile();
    }

    @Override
    public boolean supports(String profileValidatorId) {
        return getProfileId().equals(profileValidatorId);
    }

    @Override
    public String getProfileId() {
        return System.getenv(PROFILE_ID_GET_ITI_83);
    }
}
