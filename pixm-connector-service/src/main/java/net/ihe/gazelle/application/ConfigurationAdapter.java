package net.ihe.gazelle.application;

public class ConfigurationAdapter {

    public String getProfileIdCreateUpdateDeleteIti104() { return System.getenv("PROFILE_ID_CREATE_UPDATE_DELETE_ITI_104"); }
    public String getProfileIdPostIti83() {
        return System.getenv("PROFILE_ID_POST_ITI_83");
    }
    public String getProfileIdGetIti83() {
        return System.getenv("PROFILE_ID_GET_ITI_83");
    }
}
