package net.ihe.gazelle.http.validator.client.interlay.validation.ws.mock;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.http.validator.client.interlay.ws.HttpValidatorServerClientImpl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class HttpValidatorServerClientImplMock extends HttpValidatorServerClientImpl {

    String passedResponsePath = "src/test/resources/response_passed.json";
    String failedResponsePath = "src/test/resources/response_failed.json";
    String throwExceptionErrorPath = "src/test/resources/response_thrown_exception.json";

    /**
     * Default constructor for the class.
     *
     * @param serverUrl URL of the server.
     */
    public HttpValidatorServerClientImplMock(String serverUrl) {
        super(serverUrl);
    }

    public String sendMessageToValidation(String messageToValidate) {
        String response = "";

        try {
            File responseFile = new File(getResponse(messageToValidate));
            response = Files.readString(Path.of(responseFile.getAbsolutePath()));
        } catch (Exception e) {
            System.out.println("Error during process");
        }
        return response;
    }


    private String getResponse(String message) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(message);
        String profile = jsonNode.get("validationProfileId").asText();
        return "passed".equals(profile) ? passedResponsePath : "failed".equals(profile) ? failedResponsePath : throwExceptionErrorPath;

    }
}
