package net.ihe.gazelle.http.validator.client.interlay.validation;

import net.ihe.gazelle.http.validator.client.interlay.validation.mock.HttpValidationServiceMock;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationReport;
import net.ihe.gazelle.validation.api.domain.request.structure.ValidationItem;
import net.ihe.gazelle.validation.api.domain.request.structure.ValidationRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

class HttpValidationServiceTest {

    static HttpValidationServiceMock httpValidationService = new HttpValidationServiceMock();

    static ValidationRequest request = new ValidationRequest();
    static ValidationItem item = new ValidationItem();
    static File httpRequestFile = new File("src/test/resources/http_request_iti104.http");
    static String httpRequest;

    @BeforeAll
    static public void initialize() {
        try {
            httpRequest = Files.readString(Path.of(httpRequestFile.getAbsolutePath()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        item.setContent(httpRequest.getBytes());
        request.addValidationItem(item);

    }

    @Test
    void testValidatePassed() {
        request.setValidationProfileId("passed");
        ValidationReport report = httpValidationService.validate(request);
        Assertions.assertEquals("PASSED", report.getOverallResult().name());
    }

    @Test
    void testValidateFailed() {
        request.setValidationProfileId("failed");
        ValidationReport report = httpValidationService.validate(request);
        Assertions.assertEquals("FAILED", report.getOverallResult().name());
    }

    @Test
    void testValidateFailedWithCaughtJsonProcessingException() {
        request.setValidationProfileId("error");
        ValidationReport report = httpValidationService.validate(request);
        Assertions.assertEquals("FAILED", report.getOverallResult().name());
    }

    @Test
    void testGetValidationProfiles() {
        Assertions.assertInstanceOf(ArrayList.class, httpValidationService.getValidationProfiles());
    }
}