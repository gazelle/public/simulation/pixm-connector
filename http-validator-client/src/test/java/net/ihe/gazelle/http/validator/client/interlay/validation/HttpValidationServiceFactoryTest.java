package net.ihe.gazelle.http.validator.client.interlay.validation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


class HttpValidationServiceFactoryTest {

    HttpValidationServiceFactory factory = new HttpValidationServiceFactory();

    @Test
    void testGetValidationService() {
        Assertions.assertInstanceOf(HttpValidationService.class, factory.getValidationService());
    }
}