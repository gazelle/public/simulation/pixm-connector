package net.ihe.gazelle.http.validator.client.interlay.validation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.json.JsonMapper;
import net.ihe.gazelle.http.validator.client.interlay.ws.HttpValidatorServerClientImpl;
import net.ihe.gazelle.validation.api.application.ValidationService;
import net.ihe.gazelle.validation.api.domain.metadata.structure.ValidationProfile;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationReport;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationTestResult;
import net.ihe.gazelle.validation.api.domain.request.structure.ValidationItem;
import net.ihe.gazelle.validation.api.domain.request.structure.ValidationRequest;
import net.ihe.gazelle.validation.interlay.dto.report.ValidationReportDTO;
import net.ihe.gazelle.validation.interlay.dto.request.ValidationRequestDTO;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class HttpValidationService implements ValidationService {

    public static final String HTTP_VALIDATION = "http-validation";
    public static final String APP_HTTP_VALIDATOR_SERVER = System.getenv("APP_HTTP_VALIDATOR_SERVER");
    public static final String RULES_FAILED_DURING_HTTP_VALIDATION = "Some rules failed during http-validation";
    List<ValidationItem> validationItemList = new ArrayList<>();
    private ValidationReport validationReport = new ValidationReport();

    @Override
    public ValidationReport validate(ValidationRequest validationRequest) {
        ValidationRequestDTO validationRequestDTO = new ValidationRequestDTO(validationRequest);
        JsonMapper jsonMapper = new JsonMapper();
        try {
            String httpMessage = jsonMapper.writeValueAsString(validationRequestDTO);
            String response = getResponseFromHttpValidation(httpMessage);
            validationReport = jsonMapper.readValue(response, ValidationReportDTO.class);
            if (ValidationTestResult.FAILED.equals(validationReport.getOverallResult())) {
                addMessageToValidationItem(RULES_FAILED_DURING_HTTP_VALIDATION);
            }
        } catch (JsonProcessingException e) {
            validationReport.setOverallResult(ValidationTestResult.FAILED);
            addMessageToValidationItem(e.getMessage());
        } finally {
            validationReport.setValidationItems(validationItemList);
        }
        return validationReport;
    }


    protected String getResponseFromHttpValidation(String content) {
        HttpValidatorServerClientImpl httpValidatorServerClient = new HttpValidatorServerClientImpl(APP_HTTP_VALIDATOR_SERVER);
        return httpValidatorServerClient.sendMessageToValidation(content);

    }

    private void addMessageToValidationItem(String message) {
        ValidationItem vi = new ValidationItem();
        vi.setItemId(HTTP_VALIDATION);
        vi.setContent(message.getBytes(StandardCharsets.UTF_8));
        validationItemList.add(vi);
    }

    //TODO : implements this methods
    public List<ValidationProfile> getValidationProfiles() {
        return new ArrayList<>();
    }
}
